import angular from 'angular';
import uirouter from 'angular-ui-router';
import example from './components/example/example.module';
import autoCompleteTextbox from './common/autocomplete-textbox/autocomplete-textbox.module';
import wikipediaList from './components/wikipedia-list/wikipedia-list.module';
import services from './services/services.module';

require('./main.scss');
angular.module('app', [
  uirouter,
  'example',
  'autoCompleteTextbox',
  'wikipediaList',
  'services'
]);
