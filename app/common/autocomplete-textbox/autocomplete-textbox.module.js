import angular from 'angular';
import component from './autocomplete-textbox.component';
/* @ngInject */
angular
  .module('autoCompleteTextbox', [])
  .component('autoCompleteTextbox', component);
