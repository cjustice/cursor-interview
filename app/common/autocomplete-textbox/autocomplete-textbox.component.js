import autoCompleteTextboxHtml from './autocomplete-textbox.html';
import './autocomplete-textbox.scss';

/* @ngInject */
//This is in the common folder because it's something we can use across all our applications
let autoCompleteTextboxComponent = {
  template: autoCompleteTextboxHtml,
    /*
      My favorite components will give as much flexibility as possible;
      one aspect of this is including lots of hooks.
      In this case, we have a hook for "onChange", which will be
      called whenever our autocomplete textbox content is
      modified
     */
  bindings: {
    textboxModel: '=',
    debounce: '<',
    getSuggestions: '&',
    onChange: '&'
  },
  controllerAs: 'autoCompleteTextboxCtrl',
  controller: function() {
    const vm = this;

    vm.suggestions = [];

    //Our component will call this to update suggestions whenever the textbox is modified
    vm.updateSuggestions = () => {
      if (!(vm.textboxModel)) {
        vm.suggestions = [];
      } else {
        vm.suggestions = vm.getSuggestions({
          input: vm.textboxModel
        });
      }
      vm.onChange({input: vm.textboxModel});
    };

    //Our component will call this when a user clicks on a suggested word
    vm.updateValue = (suggested) => {
      vm.textboxModel = suggested;
      vm.suggestions = [];
      vm.onChange({input: vm.textboxModel});
    }
  }

};
export default autoCompleteTextboxComponent;
