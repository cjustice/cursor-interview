import wikipediaListHtml from './wikipedia-list.html';

/*
  I created this component for fun, because I wasn't sure
  what was expected out of the project. It will call the wikipedia
  API and suggest relevant pages based on the name you've entered
 */

//This is in the components folder because it's (in-theory) unique to this application.
//There's an argument to be made that it belongs in the common folder, but I prefer to keep
//stuff in there as generic as possible

/* @ngInject */
let wikipediaListComponent = {
  template: wikipediaListHtml,
  bindings: {
    searchQuery: '@'
  },
  controllerAs: 'wikipediaListCtrl',
  controller: function (wikipediaService, $log, $timeout) {
    const vm = this;
    vm.searchQuery = '';
    vm.wikiResults = [];

    //We don't want to poll the API with every keystroke, so this acts as a debounce
    vm.timeout = null;
    vm.$onChanges = (changes) => {
      $timeout.cancel(vm.timeout);
      vm.timeout = $timeout(() => {
        wikipediaService.queryPages(vm.searchQuery).then((results) => {
          vm.wikiResults = results;
        });
      }, 1000);
    }
  }

};
export default wikipediaListComponent;
