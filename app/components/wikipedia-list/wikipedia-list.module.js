import angular from 'angular';
import component from './wikipedia-list.component';
/* @ngInject */
angular
  .module('wikipediaList', [])
  .component('wikipediaList', component);
