const wikiLink = 'en.wikipedia.org/w/api.php';

/*
  The Wikipedia service is broken out of the wikipedia-list component
  because, in the future, other parts of our app may also want to call the wikipedia API
 */
function wikipediaService($http, $log) {

  return {
    queryPages: (searchTerm) =>
      $http({
        method: 'GET',
        url: `https://${wikiLink}?action=query&list=search&srsearch=${searchTerm}` +
          '&format=json&origin=*'
      })
        .then((response) => {
          let searchResults = response.data.query ? response.data.query.search : [];
          return searchResults;
        })
  }

}
/* @ngInject */
export default wikipediaService;
