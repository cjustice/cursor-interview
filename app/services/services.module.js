import angular from 'angular';
import wikipedia from './wikipedia.service';
/* @ngInject */
angular
  .module('services', [])
  .factory('wikipediaService', wikipedia);
